// Event bus

const eventBus = _.extend({}, Backbone.Events);

// Unit model

var Unit = Backbone.Model.extend({
  defaults: {
    x: 100,
    y: 100,
    idx: 0
  }
});

// Units collection

var Units = Backbone.Collection.extend({
	model: Unit
});

// 1000x1000 area view

var CanvasView = Backbone.View.extend({
  el: '#canvas',
  initialize: function(options) {
    this.eventBus = options.eventBus;
    eventBus.bind('changed', (e) =>  {
      console.log('hello');
      var idx = e.idx;
      var unitView = _.find(this.unitViews, function(item) {
          return item.model.get('idx') == idx;
      });
      unitView.model.set('x', e.x);
      unitView.model.set('y', e.y);
      unitView.render();
    });
  },
  render: function() {
    this.unitViews = [];
    this.collection.each((unit, idx) => {
      var unitView = new UnitView({ model: unit });
      unitView.model.set('idx', idx);
      this.unitViews.push(unitView);
			this.$el.append(unitView.render().el);
  	});
    this.eventBus.trigger('movement', this.unitViews[0].model.toJSON());
    return this;
  },
  events: {
    mousedown: function(e) {
      if(e.target.classList.contains('unit')) {
        var idx = $(e.target).data('id');
        var unitView = _.find(this.unitViews, function(item) {
            return item.model.get('idx') == idx;
        });
        this.dragging = unitView;
        $(e.target).removeClass('placed');
        this.dragOffsetX = e.offsetX;
        this.dragOffsetY = e.offsetY;
        e.stopPropagation();
        e.preventDefault();
      }
    },
    mousemove: function(e) {
      if(e.target.classList.contains('unit')) {
      }
      if(this.dragging) {
        this.dragging.model.set('x', e.pageX - this.$el.offset().left - this.dragOffsetX);
        this.dragging.model.set('y', e.pageY - this.$el.offset().top - this.dragOffsetY);
        this.dragging.render(true);
        this.eventBus.trigger('movement', this.dragging.model.toJSON());
        e.stopPropagation();
        e.preventDefault();
      }
    },
    mouseup: function(e) {
      if(this.dragging) {
        this.dragging = null;
        $(e.target).addClass('placed');
        e.stopPropagation();
        e.preventDefault();
      }
    }
  }
});

// Unit view

var UnitView = Backbone.View.extend({
	initialize: function(){
		this.render();
	},
	render: function(){
    var model = this.model.toJSON();
    this.$el.data('id', model.idx);
    this.$el.addClass('unit sprite');
    this.$el.attr('style', 'transform: translate('+model.x+'px,'+model.y+'px);');
    return this;
	}
});

// Form view

const FormModel = Backbone.Model.extend({
  defaults: {
    x: 0,
    y: 0
  }
});

const Form = Backbone.View.extend({
  el: '#form',
  initialize: function(options) {
    this.eventBus = options.eventBus;
    this.model = new FormModel();
    this.eventBus.bind('movement', (e) => {
      console.log("movement got",e);
      this.model.set('x', e.x);
      this.model.set('y', e.y);
      this.model.set('idx', e.idx);
      this.render();
    });
  },
  events: {
    'change input': function() {
      this.model.set('x', this.$el.find('input#inputx').val());
      this.model.set('y', this.$el.find('input#inputy').val());
      console.log("changed", this.model.toJSON());
      this.eventBus.trigger('changed', this.model.toJSON());
    }
  },
  template: _.template('<form><label for="x">X: </label><input id="inputx" name="x" value="<%= x %>"><br/><label for="y">Y: </label><input id="inputy" name="y" value="<%= y %>"></form>'),
  render: function() {
    console.dir(this.model.toJSON());
    this.$el.html(this.template(this.model.toJSON()));
    return this;
  }
});

// Setting up one unit in collection

var units = new Units([{
  x: 100,
  y: 200
}]);

// Form init and render

var form = new Form(
{
  eventBus
});

// Creating 1000x1000 area for unit

var canvasView = new CanvasView({
  eventBus,
  collection: units
});



// Appending unit canvas to document body

form.render();
canvasView.render();


console.log("started");
